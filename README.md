Forked from https://gitlab.com/uoou/dotfiles/-/blob/master/stow/bin/home/drew/.local/bin/lace

Lace is a small script to interlace Gemini Capsule micro blogs.

Enhancements made to the original lace with this fork.

The original lace would not run on Linux distributions running sed 3.7 or on Termux on Android.

Lace can now run on either a standard Linux distributions running sed 3.7, Termux on android, and UserLAnd on Android.  Sed, date, openssl, curl, less are dependencies that need to be installed.

The original script showed how long ago a entry was posted. Lace now shows how long ago a post occurred and the date and time of the post. 

### Directions

There are three bash scripts that make up the lace family, lace, laced, and lacedreader.

lace is a standalone program.

To use lace put it in your path and make it executable

lace will read subscriptions

lace nope will read subscriptions without less

lace subs will list subscriptions\n"

lace sub nickname url will - add subscription, e.g.:lace sub Drew gemini://friendo.monster/tiny.gmi

lace unsub nickname will remove subscription, e.g.:lace unsub Drew

lace update will redownload script to location of current script (might need sudo, depending on location)

---------------------------------------------
laced and lacereader are programs designed to be used together.

To use laced and lacereader put both programs in your path and make them executable

laced is designed to be executed as a cron job.  Rather than running lace and waiting for all the micro blogs to be downloaded and sorted your can run laced as a cron job.  laced will store the downloaded and sorted microblogs in a file.  lacedreader is designed to read the file and present the file to you in less.

laced has the option of "laced strict" lace and lacedadds color codes that less uses to present colors to the user.  lace and laced are programs that are meant to be hacked on to meet your needs.  I added the strict option to not add the color codes which an not Gemini compliant.  This way if you want to configure your Gemini browser to open the file, the file will appear as valid Gemini.

For lace to read microblogs they should be in the following format. You do not need to use UTC but is the peferred format.  You can use other formats such as EST or EDT and lace will do it best to calculate the time since not all time zone code abbreviations are unique.

# Fr. Robert's Microblog

## Tue 18 May 2021 19:31 UTC
I have joined the Fediverse my instance is at https://social.frrobert.com/channel/frrobert

## Tue 18 May 2021 19:29 UTC
@Toby there is there is an additional program call khard that is a contact list that works with pim utils.  It was developed by someone else but it works fine with vdirsyncer.  I have been using all the programs for several years and I second your endorsement of them.

## Mon 17 May 2021 19:32 UTC
=>gemini://frrobert.net/log/trendythoughts-2021-05-16-1607.gmi This week's Trendy Thoughts are out

----------------

How lace results appear. 

8 hours ago
Tue 18 May 2021 04:22:00 PM EDT
🚀 hexdsl (hexdsl.co.uk)
I like cheese, like, more than most

18 hours ago
Tue 18 May 2021 03:31:00 PM EDT
☦ frrobert (frrobert.net)
I have joined the Fediverse my instance is at https://social.frrobert.com/channel/frrobert

19 hours ago
Tue 18 May 2021 03:29:00 PM EDT
☦ frrobert (frrobert.net)
@Toby there is there is an additional program call khard that is a contact list that works with pim utils.  It was developed by someone else but it works fine with vdirsyncer.  I have been using all the programs for several years and I second your endorsement of them.

25 hours ago
Tue 18 May 2021 09:12:18 AM EDT
🐘 Adele (adele.work)
Hey, I test my online tinylog editor ! It works with a simple gemini browser !


### Favicons

Lace may show the actual code for a favicon rather than the icon itself depending on the version of GNU less and/or fonts installed. If this happens you can either install a font that supports the favicon or comment out the following line in the script.

```
fav=$(openssl s_client -crlf -quiet -connect "$domain:1965" <<<"gemini://$domain/favicon.txt" 2>/dev/null | sed -e '1d')
```
Since favicons are not part of the Gemini standard, you may not want favicons to display.  Commenting out the above line of code will prevent favicons from displaying.

### Updates

2012-05-?? I added the strict option to laced.  If you use laced strict the file that is created is a true gemini formatted file without the color codes used by the lacedreader.

2021-05-03 I added laced and lacedreader.  laced will download microblogs but will not open them for reading.  It is designed to be used with crontab. lacedreader is the reader to be used with the laced.

2021-04-28 I added the logic that if the server times out on the first connect to skip the second connect.

2021-04-27 I added a timeout variable in that is used with the timeout command to timeout connecting after a designated time to an unresponsive server.  If the value for the variable is a number it is in seconds if it is a number followed by a m then it is in minutes.  

### Post about original script.

https://friendo.monster/posts/a-script-to-interleave-tiny-gemini-logs.html

